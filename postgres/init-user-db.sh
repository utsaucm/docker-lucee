#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER today;
    CREATE DATABASE today;
    GRANT ALL PRIVILEGES ON DATABASE today TO today;
EOSQL
