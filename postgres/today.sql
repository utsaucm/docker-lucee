CREATE TABLE ArticleTypes (
  ArticleTypeID integer PRIMARY KEY,
  ArticleTypeName varchar(32)
);
\COPY ArticleTypes FROM 'migrate-today/csv/meta/ArticleTypes.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE Audiences (
  AudienceID integer PRIMARY KEY,
  AudienceName varchar(32)
);
\COPY Audiences FROM 'migrate-today/csv/meta/Audiences.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE Categories (
  CategoryID integer PRIMARY KEY,
  CategoryName varchar(32),
  CategoryDescription varchar(32),
  CategoryStatus boolean
);
\COPY Categories FROM 'migrate-today/csv/meta/Categories.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE Links (
  Linkid integer PRIMARY KEY,
  URL varchar(128),
  Categories varchar(32),
  Category_Description varchar(32),
  Link_Name varchar(64),
  Link_Description varchar(256),
  LinkStatus boolean,
  PrimaryCustodianID integer,
  Rank integer
);
\COPY Links FROM 'migrate-today/csv/meta/Links.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE PhotoDimensions (
  photoDimensionID integer PRIMARY KEY,
  photodimensionText varchar(12),
  photoDimensionDescription varchar(32)
);
\COPY PhotoDimensions FROM 'migrate-today/csv/meta/PhotoDimensions.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE RSSCategories (
  CategoryID integer PRIMARY KEY,
  CategoryName varchar(64),
  CategoryDescription varchar(256),
  CategoryStatus boolean
);
\COPY RSSCategories FROM 'migrate-today/csv/meta/RSSCategories.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE subAudiences (
  SubAudienceID integer PRIMARY KEY,
  SubAudienceName varchar(32)
);
\COPY subAudiences FROM 'migrate-today/csv/meta/subAudiences.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE subCategories (
  subCategoryID integer PRIMARY KEY,
  subCategoryName varchar(64)
);
\COPY subCategories FROM 'migrate-today/csv/meta/subCategories.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE AudienceArticleRelations (
  AudienceArticleRelationID integer PRIMARY KEY,
  ArticleID integer,
  AudienceID integer
);
\COPY AudienceArticleRelations FROM 'migrate-today/csv/relations/AudienceArticleRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE AudienceLinkRelations (
  AudienceLinkRelationsID integer PRIMARY KEY,
  AudienceID integer,
  LinkID integer
);
\COPY AudienceLinkRelations FROM 'migrate-today/csv/relations/AudienceLinkRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE CategoryArticleRelations (
  CategoryID integer,
  ArticleID integer,
  RelationshipID integer PRIMARY KEY
);
\COPY CategoryArticleRelations FROM 'migrate-today/csv/relations/CategoryArticleRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE CategoryLinkRelations (
  CategoryID integer,
  LinkID integer
);
\COPY CategoryLinkRelations FROM 'migrate-today/csv/relations/CategoryLinkRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE RSSCategoryArticleRelations (
  CategoryID integer,
  ArticleID integer,
  RelationshipID integer PRIMARY KEY
);
\COPY RSSCategoryArticleRelations FROM 'migrate-today/csv/relations/RSSCategoryArticleRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE subAudienceLinkRelations (
  subAudienceID integer,
  LinkID integer
);
\COPY subAudienceLinkRelations FROM 'migrate-today/csv/relations/subAudienceLinkRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE subCategoryLinkRelations (
  subCategoryID integer,
  LinkID integer
);
\COPY subCategoryLinkRelations FROM 'migrate-today/csv/relations/subCategoryLinkRelations.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE Articles (
  ArticleID integer PRIMARY KEY,
  DateAdded timestamp without time zone,
  DateStart timestamp without time zone,
  DateExpire timestamp without time zone,
  DateEmphasis timestamp without time zone,
  DateLastModified timestamp without time zone,
  Title varchar(128),
  Teaser varchar(128),
  Content varchar(2048),
  URL varchar(256),
  CategoryID integer,
  HistoricalRating boolean,
  ArticleTypeID integer,
  MarketAudienceID integer,
  CustodianLastEditID varchar(32),
  CustodianOwnderID integer,
  CustodianLastEditIP varchar(32),
  Author varchar(256),
  Submitter varchar(256),
  leadPhoto integer,
  leadPhotoAddress varchar(256),
  leadPhotoAddress2 varchar(256),
  internalStory boolean,
  aroundCampus integer,
  moreNewsStory integer
);
\COPY Articles FROM 'migrate-today/csv/Articles.csv' DELIMITER ',' CSV HEADER;
